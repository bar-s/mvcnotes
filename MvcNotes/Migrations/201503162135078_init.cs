namespace MvcNotes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ParentId = c.Int(),
                        OwnerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.ParentId)
                .ForeignKey("dbo.Users", t => t.OwnerId, cascadeDelete: true)
                .Index(t => t.ParentId)
                .Index(t => t.OwnerId);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Value = c.String(nullable: false),
                        OwnerInformation = c.String(),
                        CreateDate = c.DateTime(nullable: false, storeType: "datetime2"),
                        OwnerId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.OwnerId)
                .Index(t => t.OwnerId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NoteCategories",
                c => new
                    {
                        Note_Id = c.Int(nullable: false),
                        Category_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Note_Id, t.Category_Id })
                .ForeignKey("dbo.Notes", t => t.Note_Id, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.Note_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.UserNotes",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Note_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Note_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Notes", t => t.Note_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Note_Id);
            
            CreateTable(
                "dbo.TagNotes",
                c => new
                    {
                        Tag_Id = c.Int(nullable: false),
                        Note_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_Id, t.Note_Id })
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .ForeignKey("dbo.Notes", t => t.Note_Id, cascadeDelete: true)
                .Index(t => t.Tag_Id)
                .Index(t => t.Note_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TagNotes", new[] { "Note_Id" });
            DropIndex("dbo.TagNotes", new[] { "Tag_Id" });
            DropIndex("dbo.UserNotes", new[] { "Note_Id" });
            DropIndex("dbo.UserNotes", new[] { "User_Id" });
            DropIndex("dbo.NoteCategories", new[] { "Category_Id" });
            DropIndex("dbo.NoteCategories", new[] { "Note_Id" });
            DropIndex("dbo.Notes", new[] { "OwnerId" });
            DropIndex("dbo.Categories", new[] { "OwnerId" });
            DropIndex("dbo.Categories", new[] { "ParentId" });
            DropForeignKey("dbo.TagNotes", "Note_Id", "dbo.Notes");
            DropForeignKey("dbo.TagNotes", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.UserNotes", "Note_Id", "dbo.Notes");
            DropForeignKey("dbo.UserNotes", "User_Id", "dbo.Users");
            DropForeignKey("dbo.NoteCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.NoteCategories", "Note_Id", "dbo.Notes");
            DropForeignKey("dbo.Notes", "OwnerId", "dbo.Users");
            DropForeignKey("dbo.Categories", "OwnerId", "dbo.Users");
            DropForeignKey("dbo.Categories", "ParentId", "dbo.Categories");
            DropTable("dbo.TagNotes");
            DropTable("dbo.UserNotes");
            DropTable("dbo.NoteCategories");
            DropTable("dbo.Tags");
            DropTable("dbo.Users");
            DropTable("dbo.Notes");
            DropTable("dbo.Categories");
        }
    }
}
