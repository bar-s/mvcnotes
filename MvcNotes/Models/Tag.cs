﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvcNotes.Models;

namespace MvcNotes.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
    }
}
