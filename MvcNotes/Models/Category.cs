﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcNotes.Models
{
    public class Category
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Наименование категории необходимо")]
        [Display(Name = "Наименование категории")]
        public string Name { get; set; }
        public int? ParentId { get; set; }
        [ForeignKey("ParentId")]
        public virtual ICollection<Category> SubCategories { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public User Owner { get; set; }
        public int OwnerId { get; set; }
    }
}
