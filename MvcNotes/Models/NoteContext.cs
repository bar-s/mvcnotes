﻿using System.Data.Entity;

namespace MvcNotes.Models
{
    public class NoteContext:DbContext
    {
        public NoteContext()
            : base("DefaultConnection")
        {
        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
