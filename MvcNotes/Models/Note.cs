﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MvcNotes.Models
{
    public class Note
    {
        public int Id { get; set; }
        [Display(Name = "Название заметки")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Содержимое заметки необходимо")]
        [Display(Name = "Содержимое заметки")]
        public string Value { get; set; }
        [Display(Name = "Информация о создателе")]
        public string OwnerInformation { get; set; }
        [Column(TypeName = "DateTime2")]
        [Display(Name = "Дата создания")]
        public DateTime CreateDate { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
        [ForeignKey("OwnerId")]
        public virtual User Owner { get; set; }
        public int? OwnerId { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

        public virtual ICollection<User> AuthUsers { get; set; }
        public bool IsPublic = false;
    }
}
