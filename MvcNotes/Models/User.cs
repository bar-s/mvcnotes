﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using MvcNotes.Models;

namespace MvcNotes.Models
{
   public  class User
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        [InverseProperty("AuthUsers")]
        public virtual ICollection<Note> AuthNotes { get; set; }
        [InverseProperty("Owner")]
        public virtual ICollection<Note> UserNotes { get; set; }
        public virtual ICollection<Category> UserCategories { get; set; }
    }
}
