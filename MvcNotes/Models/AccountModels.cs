﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace MvcNotes.Models
{ 
    public class RegisterExternalLoginModel
    {
        [Required(ErrorMessage = "Имя пользователя необходимо")]
        [Display(Name = "Имя пользователя")]
        public string Name { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required(ErrorMessage = "Текущий пароль необходим")]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Новый пароль необходим")]
        [StringLength(100, ErrorMessage = "Новый пароль должен быть длиной не менее {2}", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и подтверждение не совпадают")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Имя пользователя необходимо")]
        [Display(Name = "Имя пользователя:")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Пароль необходим")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль:")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Имя пользователя необходимо")]
        [Display(Name = "Имя пользователя:")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Пароль необходим")]
        [StringLength(100, ErrorMessage = "Пароль должен быть не менее  {2} символов", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль:")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
