﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcNotes.Models;
using MvcNotes.Repository;

namespace MvcNotes.Controllers
{

    [Authorize]
    public class MyNotesController : Controller
    {
        private NotesRepository db = new NotesRepository();
        //
        // GET: /MyNotes/

        public ActionResult Index()
        {
            int userId = (int)Membership.GetUser().ProviderUserKey;
            List<Category> lstCats = db.GelAllCategories(userId);
            return View(lstCats);
        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
