﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcNotes.Models;
using MvcNotes.Repository;

namespace MvcNotes.Controllers
{
    [Authorize]
    public class NotesController : Controller
    {
        private NotesRepository db = new NotesRepository();

        //
        // GET: /Notes/

        public ActionResult Index(int id=0)
        {
            int userId = (int)Membership.GetUser().ProviderUserKey;
            List<Note> notes = new List<Note>();
            if(id == 0)
            {
                ViewBag.Category = "Заметки без категории:";
                notes = db.GetAllNotesWithoutCategory(userId);
            }
            else
            {
                Category category = db.FindCategory(id);
                ViewBag.Category = String.Format("Заметки в категории {0}:",category.Name);
                notes = db.GetAllCategoryNotes(userId, id);
            }
            return View(notes);
        }


        [HttpGet]
        public ActionResult Create()
        {
            int userId = (int)Membership.GetUser().ProviderUserKey;
            var categories = db.GelAllCategories(userId);
            ViewBag.Categories = categories;
            return View();
        }

        //
        // POST: /Notes/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Note note, int[] categoriesID)
        {
            if (ModelState.IsValid)
            {
                if (categoriesID != null)
                {
                    List<Category> lstCategories = new List<Category>();
                    foreach (int id in categoriesID)
                    {
                        Category category = db.FindCategory(id);
                        lstCategories.Add(category);
                    }
                    note.Categories = lstCategories;
                }
                db.AddNote(note);
                db.Save();
                return RedirectToAction("Index","MyNotes");
            }
            int userId = (int)Membership.GetUser().ProviderUserKey;
            var categories = db.GelAllCategories(userId);
            ViewBag.Categories = categories;
            return View(note);
        }

        //
        // GET: /Notes/Edit/5
        
        public ActionResult Edit(int Id)
        {
            Note note = db.FindNote(Id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        //
        // POST: /Notes/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Note note)
        {
            if (ModelState.IsValid)
            {
                db.EditNote(note);
                db.Save();
                return RedirectToAction("Index","MyNotes");
            }
            //ViewBag.OwnerId = new SelectList(db.Users, "Id", "Name", note.OwnerId);
            return View(note);
        }
        
        //
        // GET: /Notes/Delete/5

        public ActionResult Delete(int id)
        {
            Note note = db.FindNote(id);
            if (note == null)
            {
                return HttpNotFound();
            }
            return View(note);
        }

        //
        // POST: /Notes/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.RemoveNote(id);
            db.Save();
            return RedirectToAction("Index","MyNotes");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}