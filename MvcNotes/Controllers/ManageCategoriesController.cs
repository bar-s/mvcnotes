﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MvcNotes.Models;
using MvcNotes.Repository;

namespace MvcNotes.Controllers
{
    [System.Web.Mvc.Authorize]
    public class ManageCategoriesController : Controller
    {
        private NotesRepository db = new NotesRepository();

        //
        // GET: /ManageCategories/

        public ActionResult Index()
        {
            int userId = (int)Membership.GetUser().ProviderUserKey;
            List<Category> lstCats = db.GelAllCategories(userId);
            return View(lstCats);
        }
        [HttpGet]
        public ActionResult AddSubCategory(int id = 0)
        {
            Category subCategory = new Category() {ParentId = id};
            return View(subCategory);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSubCategory(Category category)
        {
            if (ModelState.IsValid)
            {
                int userId = (int)Membership.GetUser().ProviderUserKey;
                User owner = db.FindUser(userId);
                category.Owner = owner;
                db.AddSubCategory(category);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(category);
        }

    

        //
        // GET: /ManageCategories/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ManageCategories/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Category category)
        {
            if (ModelState.IsValid)
            {
                int userId = (int)Membership.GetUser().ProviderUserKey;
                User owner = db.FindUser(userId);
                category.Owner = owner;
                db.AddCategory(category);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(category);
        }
        
        //
        // GET: /ManageCategories/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Category category = db.FindCategory(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        //
        // POST: /ManageCategories/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Category category)
        {
            if (ModelState.IsValid)
            {
                db.EditCategory(category);
                db.Save();
                return RedirectToAction("Index");
            }
            return View(category);
        }
        
        //
        // GET: /ManageCategories/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Category category = db.FindCategory(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        //
        // POST: /ManageCategories/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.RemoveCategoryAndSubWithNotes(id);
            db.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}