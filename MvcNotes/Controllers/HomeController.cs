﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcNotes.Models;
using MvcNotes.Repository;

namespace MvcNotes.Controllers
{
    public class HomeController : Controller
    {
        //private NotesRepository db = new NotesRepository();
        public ActionResult Index()
        {
            ViewBag.Message = "Привет! Это простой сервис для хранения ваших заметок.";

            return View();
        }

        public ActionResult About()
        {
            /*User user1 = db.FindUser(1);
            Category cat1 = new Category
            {
                Name = "ITMO",
                Owner = user1,
                SubCategories = new List<Category>
                {
                    new Category
                    {
                        Name = "TPS",
                        Owner = user1
                    }
                }
            };
            db.AddCategory(cat1);
            db.Save();*/
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
         /*protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }*/
    }
}
