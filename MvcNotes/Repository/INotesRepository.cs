﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvcNotes.Models;

namespace MvcNotes.Repository
{
    interface INotesRepository:IDisposable
    {
        void AddNote(Note newNote);
        void AddCategory(Category newCat);
        void UpdateNote(int id, string newName, string newValue, List<Tag> newTags, List<User> newAuthUsers);
        void UpdateCategory(int id, string newName);
        void SelectSubCategories(int id);
        void SelectAllCategories();
        void RemoveNote(int id);
        void RemoveCategoryAndSubWithNotes(int id);
        void ChangeNoteCategory(int noteId, int oldCategoryId, int newCategoryId);
        void ChangeCategoryParent(int categoryId, int newParentId);
    }
}
