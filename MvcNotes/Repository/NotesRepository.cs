﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcNotes.Models;
using System.Data.Entity;
using System.Web.Security;
using System.Data;

namespace MvcNotes.Repository
{
    public class NotesRepository:INotesRepository
    {
        private readonly NoteContext db;
        public NotesRepository()
        {
            this.db=new NoteContext();
        }
        public void Save()
        {
            db.SaveChanges();
        }

        private bool _disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<Note> GetAllNotesWithoutCategory(int userID)
        {
            var notes = db.Notes.Where(n => n.OwnerId == userID).Include(n => n.Categories);
            List<Note> resultNotes = new List<Note>();
            foreach (var note in notes)
            {
                if (note.Categories.Count==0)
                {
                    resultNotes.Add(note);
                }
            }
            return resultNotes;
        }

        public List<Note> GetAllCategoryNotes(int userID, int categoryID)
        {
            Category category = db.Categories.Find(categoryID);
            var notes = db.Notes.Where(n => n.OwnerId == userID).Include(n => n.Categories);
            List<Note> categoryNotes = new List<Note>();
            foreach (var note in notes)
            {
                if (note.Categories.Contains(category))
                {
                    categoryNotes.Add(note);
                }
            }
            return categoryNotes;
        }

        public List<Category> GelAllCategories(int userID)
        {
            return db.Categories.Where(c => c.OwnerId == userID).ToList();
        }

        public Note FindNote(int noteID)
        {
            return db.Notes.Find(noteID);
        }

        public User FindUser(int userID)
        {
            return db.Users.Find(userID);
        }

        public Category FindCategory(int categoryID)
        {
            return db.Categories.Find(categoryID);
        }
        public void EditNote(Note note)
        {
            db.Entry(note).State = EntityState.Modified;
        }
        public void EditCategory(Category category)
        {
            db.Entry(category).State = EntityState.Modified;
        }


        public void AddSubCategory(Category subCategory)
        {
            var parentCategory = db.Categories.Where(c=>c.Id==subCategory.ParentId).Include(c=>c.SubCategories).FirstOrDefault();
            if (parentCategory.SubCategories == null)
            {
                List<Category> lstCats=new List<Category>();
                lstCats.Add(subCategory);
            }
            else
            {
                parentCategory.SubCategories.Add(subCategory);
            }
        }
        public void AddNote(Note newNote)
        {
            int userID = (int)Membership.GetUser().ProviderUserKey;
            User owner = db.Users.Find(userID);
            newNote.Owner = owner;
            newNote.CreateDate = DateTime.UtcNow.AddHours(3);
            db.Notes.Add(newNote); 
        }
        public void AddCategory(Category newCat)
        {
            db.Categories.Add(newCat);
        }
        public void UpdateCategory(int id, string newName)
        {
            var cat = db.Categories.Find(id);
            cat.Name = newName;
        }
        public void UpdateNote(int id, string newName, string newValue, List<Tag> newTags, List<User> newAuthUsers)
        {
            var note = db.Notes.Find(id);
            note.Name = newName;
            note.Value = newValue;
            note.Tags = newTags;
            note.AuthUsers = newAuthUsers;        
        }       
        private static string temp = "";
        public void SelectSubCategories(int id)
        {            
            var category = db.Categories.Where(c=>c.Id==id).Include(c=>c.SubCategories).FirstOrDefault();
            Console.WriteLine(category.Name);
            if (category.SubCategories != null)
            {
                temp += "-";
                foreach (Category subCategory in category.SubCategories)
                {
                    Console.Write(temp);
                    SelectSubCategories(subCategory.Id);
                }
                temp=temp.Remove(temp.Length - 1);
            }
        }
        
        public void SelectAllCategories()
        {
                var categories = db.Categories.Where(c => c.ParentId == null);
                foreach (var category in categories)
                {
                    SelectSubCategories(category.Id);
                }  
        }

        public void RemoveNote(int id)
        {
            var removeNote = db.Notes.Find(id);
            db.Notes.Remove(removeNote);
        }


        public void RemoveCategoryAndSubWithNotes(int id)
        {
            var category = db.Categories.Where(c => c.Id == id).Include(c => c.SubCategories).FirstOrDefault();
            if (category.SubCategories != null)
            {
                var tempSubCategories = category.SubCategories;
                category.SubCategories = null;
                foreach (Category subCategory in tempSubCategories)
                {
                    RemoveCategoryAndSubWithNotes(subCategory.Id);
                }
            }
            category.Notes = null;
            db.Categories.Remove(category);        
        }

        public void ChangeNoteCategory(int noteId, int oldCategoryId,int newCategoryId)
        {
            var oldCategory = db.Categories.Where(c => c.Id == oldCategoryId).Include(c => c.Notes).FirstOrDefault(); 
            var note = db.Notes.Find(noteId);
            oldCategory.Notes.Remove(note);
            var newCategory = db.Categories.Where(c => c.Id == newCategoryId).Include(c => c.Notes).FirstOrDefault();
            if (newCategory != null)
            {
                newCategory.Notes.Add(note);
            }
            else
            {
                var lstNotes = new List<Note> {note};
                newCategory.Notes = lstNotes;
            }     
        }

        public void ChangeCategoryParent(int categoryId, int newParentId)
        {
            var category = db.Categories.Find(categoryId);
            var oldParent = db.Categories.Where(c => c.Id == category.ParentId).Include(c => c.SubCategories).FirstOrDefault();
            oldParent.SubCategories.Remove(category);

            var newParent = db.Categories.Where(c => c.Id == newParentId).Include(c => c.SubCategories).FirstOrDefault();

            if (newParent.SubCategories == null)
            {
                var lstCat = new List<Category> {category};
                newParent.SubCategories = lstCat;
            }
            else
            {
                newParent.SubCategories.Add(category);
            }
        }
    }
}